package com.xverion.main;

import com.xverion.main.listener.*;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.*;

import java.util.logging.Logger;

/**
 * Created by brian on 25-9-2015.
 */
public class QuarryUplink extends JavaPlugin
{

    public static QuarryUplink plugin;
    public final Logger logger = Logger.getLogger("Minecraft");
    public boolean inLobby;
    public boolean inGame;
    public boolean postGame;
    //custom ball
    public ItemStack ball = new ItemStack(Material.FIREBALL);
    public Inventory inv;

    //scoreboard managment

    public Team redTeam;
    public Team blueTeam;
    public Scoreboard board;
    public ScoreboardManager scoreboardManager;
    //objective Handling

    public Objective objective;

    public Score scoreRed;
    public Score scoreBlue;

    //team managment
    public Location blueSpawnLocation;
    public Location redSpawnLocation;
    public Location lobbySpawn;

    
    @Override
    public void onEnable() {
        super.onEnable();

        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        inLobby = true;

        ScoreboardManager sm = Bukkit.getScoreboardManager();
        Scoreboard sb = sm.getNewScoreboard();
        Team red = sb.registerNewTeam("Red");
        Team blue = sb.registerNewTeam("Blue");

        Objective objective = sb.registerNewObjective("Score", "dummy");

        Score scorered = objective.getScore(ChatColor.RED + "RED: ");
        Score scoreblue = objective.getScore(ChatColor.BLUE + "BLUE: ");

        scorered.setScore(0);
        scoreblue.setScore(0);

        this.setBlueTeam(blue);
        this.setRedTeam(red);
        this.setScoreboard(sb);
        this.setScoreboardManager(sm);
        this.setObjective(objective);
        this.setBlueScore(scoreblue);
        this.setRedScore(scorered);

        //Team red
        red.setAllowFriendlyFire(false);
        red.setDisplayName("Team Red");

        //Team blue
        blue.setAllowFriendlyFire(false);
        blue.setDisplayName("Team Blue");

        //Objective handling
        objective.setDisplayName("Score: ");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);


        PluginDescriptionFile pdffile = getDescription();

        logger.info(pdffile.getName() + " " + pdffile.getVersion() + "Has been enabled");

        Bukkit.getPluginManager().registerEvents(new onPlayerJoinEvent(this) ,this);
        Bukkit.getPluginManager().registerEvents(new onPlayerQuitEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onPlayerDeathEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onDamageEvent(this), this);
        Bukkit.getPluginManager().registerEvents(new onBlockBreak(this), this);

        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Scheduler(this), 20, 20);
    }

    public void setBlueTeam(Team team)
    {
        redTeam = team;
    }

    public void setRedTeam(Team team)
    {
        blueTeam = team;
    }

    public void setScoreboard(Scoreboard scoreboard)
    {
        board = scoreboard;
    }

    public void setScoreboardManager(ScoreboardManager manager)
    {
        scoreboardManager = manager;
    }

    public void setObjective(Objective objective)
    {
        this.objective = objective;
    }

    public void setBlueScore(Score score)
    {
        scoreBlue = score;
    }

    public void setRedScore(Score score)
    {
        scoreRed = score;
    }

    @Override
    public void onDisable() {
        super.onDisable();

        PluginDescriptionFile pdffile = getDescription();

        logger.info(pdffile.getName() + " " + pdffile.getVersion() + "Has been diabled");

        Bukkit.getServer().getScheduler().cancelTasks(this);

    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {


        if(command.getName().equalsIgnoreCase("Uplink") && sender instanceof Player)
        {
            Player player = (Player)sender;


            if(args[0].equalsIgnoreCase("setspawn"))
            {
                if(args[1].equalsIgnoreCase("lobby"))
                {
                    lobbySpawn = player.getLocation();
                    lobbySpawn.add(0, 1, 0);
                    player.sendMessage(ChatColor.DARK_RED + "Lobby spawn set.");
                    return true;
                }

                else if(args[1].equalsIgnoreCase("red"))
                {
                    redSpawnLocation = player.getLocation();
                    redSpawnLocation.add(0, 1, 0);
                    player.sendMessage(ChatColor.DARK_RED + "Red team spawn set.");
                    return true;
                }

                else if(args[1].equalsIgnoreCase("blue"))
                {
                    blueSpawnLocation = player.getLocation();
                    blueSpawnLocation.add(0, 1, 0);
                    player.sendMessage(ChatColor.DARK_RED + "Blue team spawn set.");
                    return true;
                }

                else if(args[1].equalsIgnoreCase(""))
                {
                    player.sendMessage("Not enough arguments");
                    return true;
                }
            }

            if(args[0].equalsIgnoreCase("score"))
            {
                if (args[1].equalsIgnoreCase("red"))
                {
                    int score = scoreRed.getScore() + 1;
                    scoreRed.setScore(score);
                    return true;
                }

                else if (args[1].equalsIgnoreCase("blue"))
                {
                    int score = scoreBlue.getScore() + 1;
                    scoreBlue.setScore(score);
                    return true;
                }

                else if(args[0].equalsIgnoreCase(""))
                {
                    player.sendMessage("Not enough arguments!");
                    return true;
                }

            }

            if (args[0].equalsIgnoreCase("finish"))
            {
                this.inLobby = false;
                this.inGame = false;
                this.postGame = true;
            }

            else if(args[0].equalsIgnoreCase(""))
            {
                player.sendMessage("Not enough arguments!");
                return true;
            }


        }

        return false;
    }

    public void startGame()
    {
        if(inLobby)
        {
            inLobby = false;
            inGame = true;

            for (Player players : Bukkit.getServer().getOnlinePlayers())
            {
                if (redTeam.hasEntry(players.getDisplayName()))
                {

                    players.teleport(redSpawnLocation);
                    players.setFoodLevel(100);
                    players.getInventory().addItem(new ItemStack(Material.IRON_SWORD));
                    players.getInventory().addItem(new ItemStack(Material.BOW));
                    players.getInventory().addItem(new ItemStack(Material.ARROW, 16));

                    ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
                    LeatherArmorMeta armorMetaHelmet = (LeatherArmorMeta) helmet.getItemMeta();
                    armorMetaHelmet.setColor(Color.RED);
                    helmet.setItemMeta(armorMetaHelmet);

                    ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE);
                    LeatherArmorMeta armorMetaChest = (LeatherArmorMeta) chest.getItemMeta();
                    armorMetaChest.setColor(Color.RED);
                    chest.setItemMeta(armorMetaChest);

                    ItemStack pants = new ItemStack(Material.LEATHER_LEGGINGS);
                    LeatherArmorMeta armorMetaPants = (LeatherArmorMeta) pants.getItemMeta();
                    armorMetaPants.setColor(Color.RED);
                    pants.setItemMeta(armorMetaPants);

                    ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
                    LeatherArmorMeta armorMetaBoots = (LeatherArmorMeta) boots.getItemMeta();
                    armorMetaBoots.setColor(Color.RED);
                    boots.setItemMeta(armorMetaBoots);

                    players.getInventory().setArmorContents(new ItemStack[]{helmet, chest, pants, boots});

                }

                else if(blueTeam.hasEntry(players.getDisplayName()))
                {
                    players.teleport(blueSpawnLocation);
                    players.setFoodLevel(100);

                    players.getInventory().addItem(new ItemStack(Material.IRON_SWORD));
                    players.getInventory().addItem(new ItemStack(Material.BOW));
                    players.getInventory().addItem(new ItemStack(Material.ARROW, 16));


                    ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
                    LeatherArmorMeta armorMetaHelmet = (LeatherArmorMeta) helmet.getItemMeta();
                    armorMetaHelmet.setColor(Color.BLUE);
                    helmet.setItemMeta(armorMetaHelmet);

                    ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE);
                    LeatherArmorMeta armorMetaChest = (LeatherArmorMeta) chest.getItemMeta();
                    armorMetaChest.setColor(Color.BLUE);
                    chest.setItemMeta(armorMetaChest);

                    ItemStack pants = new ItemStack(Material.LEATHER_LEGGINGS);
                    LeatherArmorMeta armorMetaPants = (LeatherArmorMeta) pants.getItemMeta();
                    armorMetaPants.setColor(Color.BLUE);
                    pants.setItemMeta(armorMetaPants);

                    ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
                    LeatherArmorMeta armorMetaBoots = (LeatherArmorMeta) boots.getItemMeta();
                    armorMetaBoots.setColor(Color.BLUE);
                    boots.setItemMeta(armorMetaBoots);

                    players.getInventory().setArmorContents(new ItemStack[]{helmet, chest, pants, boots});
                }
            }
        }
    }


}
