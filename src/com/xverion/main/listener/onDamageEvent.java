package com.xverion.main.listener;

import com.xverion.main.QuarryUplink;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by brian on 26-9-2015.
 */
public class onDamageEvent implements Listener {

    public QuarryUplink plugin;

    public onDamageEvent(QuarryUplink pl)
    {
        plugin = pl;
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event)
    {
        if (!plugin.inGame)
        {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamageByBlock(EntityDamageEvent event)
    {
        if(event.getCause().equals(EntityDamageEvent.DamageCause.FALL))
        {
            event.setCancelled(true);
        }
    }


}
