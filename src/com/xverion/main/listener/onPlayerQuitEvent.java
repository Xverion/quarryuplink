package com.xverion.main.listener;

import com.xverion.main.QuarryUplink;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by brian on 25-9-2015.
 */
public class onPlayerQuitEvent implements Listener
{

    public QuarryUplink plugin;

    public onPlayerQuitEvent(QuarryUplink pl)
    {
        plugin = pl;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event)
    {
        Player player = event.getPlayer();

        for (Player players : Bukkit.getOnlinePlayers())
        {
            if(plugin.redTeam.hasEntry(player.getDisplayName()))
            {
                plugin.redTeam.removeEntry(player.getDisplayName());
                player.setScoreboard(plugin.scoreboardManager.getNewScoreboard());
            }

            else if(plugin.blueTeam.hasEntry(player.getDisplayName()))
            {
                plugin.blueTeam.removeEntry(player.getDisplayName());
                player.setScoreboard(plugin.scoreboardManager.getNewScoreboard());
            }
        }
    }

}
