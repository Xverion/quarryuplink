package com.xverion.main.listener;

import com.xverion.main.QuarryUplink;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 * Created by brian on 26-9-2015.
 */
public class onPlayerDeathEvent implements Listener {

    public QuarryUplink plugin;

    public onPlayerDeathEvent(QuarryUplink pl)
    {
        plugin = pl;
    }


    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event)
    {
        final Player player = event.getEntity();
        //event.setKeepInventory(true);

        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                if(player.isDead())
                {

                    if(plugin.redTeam.hasEntry(player.getDisplayName()))
                    {
                        int score = plugin.scoreBlue.getScore() + 1;
                        plugin.scoreBlue.setScore(score);

                        if(player.getInventory().getItemInHand().equals(plugin.ball))
                        {
                            player.getWorld().dropItem(player.getLocation(), plugin.ball);
                        }

                        player.setHealth(20);
                        player.setFoodLevel(100);
                        player.teleport(plugin.redSpawnLocation);

                        player.getInventory().clear();

                        player.getInventory().addItem(new ItemStack(Material.IRON_SWORD));
                        player.getInventory().addItem(new ItemStack(Material.BOW));
                        player.getInventory().addItem(new ItemStack(Material.ARROW, 16));


                        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
                        LeatherArmorMeta armorMetaHelmet = (LeatherArmorMeta) helmet.getItemMeta();
                        armorMetaHelmet.setColor(Color.RED);
                        helmet.setItemMeta(armorMetaHelmet);

                        ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE);
                        LeatherArmorMeta armorMetaChest = (LeatherArmorMeta) chest.getItemMeta();
                        armorMetaChest.setColor(Color.RED);
                        chest.setItemMeta(armorMetaChest);

                        ItemStack pants = new ItemStack(Material.LEATHER_LEGGINGS);
                        LeatherArmorMeta armorMetaPants = (LeatherArmorMeta) pants.getItemMeta();
                        armorMetaPants.setColor(Color.RED);
                        pants.setItemMeta(armorMetaPants);

                        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
                        LeatherArmorMeta armorMetaBoots = (LeatherArmorMeta) boots.getItemMeta();
                        armorMetaBoots.setColor(Color.RED);
                        boots.setItemMeta(armorMetaBoots);

                        player.getInventory().setArmorContents(new ItemStack[]{helmet, chest, pants, boots});
                    }

                    else if(plugin.blueTeam.hasEntry(player.getDisplayName()))
                    {

                        int score = plugin.scoreRed.getScore() + 1;
                        plugin.scoreRed.setScore(score);

                        if(player.getInventory().getItemInHand().equals(plugin.ball))
                        {
                            player.getWorld().dropItem(player.getLocation(), plugin.ball);
                        }


                        player.setHealth(20);
                        player.setFoodLevel(100);
                        player.teleport(plugin.blueSpawnLocation);

                        player.getInventory().clear();

                        player.getInventory().addItem(new ItemStack(Material.IRON_SWORD));
                        player.getInventory().addItem(new ItemStack(Material.BOW));
                        player.getInventory().addItem(new ItemStack(Material.ARROW, 16));


                        ItemStack helmet = new ItemStack(Material.LEATHER_HELMET);
                        LeatherArmorMeta armorMetaHelmet = (LeatherArmorMeta) helmet.getItemMeta();
                        armorMetaHelmet.setColor(Color.BLUE);
                        helmet.setItemMeta(armorMetaHelmet);

                        ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE);
                        LeatherArmorMeta armorMetaChest = (LeatherArmorMeta) chest.getItemMeta();
                        armorMetaChest.setColor(Color.BLUE);
                        chest.setItemMeta(armorMetaChest);

                        ItemStack pants = new ItemStack(Material.LEATHER_LEGGINGS);
                        LeatherArmorMeta armorMetaPants = (LeatherArmorMeta) pants.getItemMeta();
                        armorMetaPants.setColor(Color.BLUE);
                        pants.setItemMeta(armorMetaPants);

                        ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
                        LeatherArmorMeta armorMetaBoots = (LeatherArmorMeta) boots.getItemMeta();
                        armorMetaBoots.setColor(Color.BLUE);
                        boots.setItemMeta(armorMetaBoots);

                        player.getInventory().setArmorContents(new ItemStack[]{helmet, chest, pants, boots});
                    }
                }
            }
        });
    }
}
