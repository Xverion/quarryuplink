package com.xverion.main.listener;

import com.xverion.main.QuarryUplink;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

/**
 * Created by brian on 25-9-2015.
 */
public class onPlayerJoinEvent implements Listener {

    public QuarryUplink plugin;

    public onPlayerJoinEvent(QuarryUplink pl)
    {
        plugin = pl;
    }

    @EventHandler
    public void onPlayerJoin(org.bukkit.event.player.PlayerJoinEvent event) {
        plugin.logger.info("Event fired!");

        if (plugin.inLobby) {
            int i = 0;
            for (Player players : Bukkit.getOnlinePlayers()) {

                if (i % 2 == 0 && !plugin.redTeam.hasEntry(players.getDisplayName())) {
                    plugin.redTeam.addEntry(players.getDisplayName());
                    plugin.logger.info("Added to team red");
                    plugin.redTeam.setPrefix(ChatColor.RED + "");
                    players.getInventory().clear();
                    players.getInventory().setArmorContents(new ItemStack[]{new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)});

                    if(plugin.lobbySpawn != null){players.teleport(plugin.lobbySpawn);}
                    else{players.teleport(players.getWorld().getSpawnLocation());}

                } else if (i % 2 == 1 && !plugin.blueTeam.hasEntry(players.getDisplayName())){
                    plugin.blueTeam.addEntry(players.getDisplayName());
                    plugin.logger.info("Added to team blue");
                    plugin.blueTeam.setPrefix(ChatColor.BLUE + "");
                    players.getInventory().clear();
                    players.getInventory().setArmorContents(new ItemStack[]{new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)});

                    if(plugin.lobbySpawn != null){players.teleport(plugin.lobbySpawn);}
                    else{players.teleport(players.getWorld().getSpawnLocation());}
                }

                players.setScoreboard(plugin.board);

                i++;
            }
        }
    }
}
