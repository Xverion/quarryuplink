package com.xverion.main.listener;

import com.xverion.main.QuarryUplink;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by brian on 26-9-2015.
 */
public class onBlockBreak implements Listener
{
    public QuarryUplink plugin;

    public onBlockBreak(QuarryUplink pl)
    {
        plugin = pl;
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event)
    {
        if(!event.getPlayer().getGameMode().equals(GameMode.CREATIVE))
        {
            event.setCancelled(true);
        }
    }

}
