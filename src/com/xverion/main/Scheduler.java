package com.xverion.main;


import com.connorlinfoot.titleapi.TitleAPI;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by brian on 26-9-2015 for the quarry.
 */
public class Scheduler extends BukkitRunnable {

    public QuarryUplink plugin;

    public int seconds;
    public int playTime;
    public int postTime;

    boolean messageshown;

    public Scheduler(QuarryUplink pl)
    {
        plugin = pl;
        seconds = 60;
    }

    @Override
    public void run() {

      if (plugin.inLobby)
      {
          if(Bukkit.getServer().getOnlinePlayers().size() >= 2 && seconds > 0)
          {
              seconds--;

              if (seconds == 30)
              {
                  for(Player player: Bukkit.getOnlinePlayers())
                  {
                      TitleAPI.sendTitle(player, 20, 40, 20, ChatColor.YELLOW + "" + seconds + " seconds remaining!", "");
                  }
              }

              else if (seconds <= 10)
              {
                  for (Player player : Bukkit.getServer().getOnlinePlayers())
                  {
                      TitleAPI.sendTitle(player, 5, 10, 5,ChatColor.YELLOW + "" + seconds + " seconds remaining!", "");
                  }
              }
          }

          else if (seconds == 0)
          {
            if(plugin.redSpawnLocation != null && plugin.blueSpawnLocation != null) {

                plugin.startGame();
                playTime = 0;
                postTime = 0;
                messageshown = false;
            }
              else
            {
                for(Player player : Bukkit.getServer().getOnlinePlayers())
                {
                    player.sendMessage("No spawn points set! Contact admin immediately!");
                }

                seconds = 60;
            }
          }

          else
          {
              seconds = 60;
          }
      }

      else if(plugin.inGame)
      {
          if (plugin.scoreBlue.getScore() == 10)
          {
              plugin.inGame = false;
              plugin.postGame = true;

          }

          else if (plugin.scoreRed.getScore() == 10)
          {

              plugin.inGame = false;
              plugin.postGame = true;

          }

          else if(playTime == 120)//change back to 10 minutes 600seconds
          {
              plugin.inGame = false;
              plugin.postGame = true;

          }

          playTime++;
      }

      else if (plugin.postGame)
      {

          int redScore = plugin.scoreRed.getScore();
          int blueScore = plugin.scoreBlue.getScore();

          for(Player player : Bukkit.getServer().getOnlinePlayers())
          {
              player.getInventory().clear();
              player.getInventory().setArmorContents(new ItemStack[]{new ItemStack(Material.AIR), new ItemStack(Material.AIR) ,new ItemStack(Material.AIR) ,new ItemStack(Material.AIR) });
          }

          if(redScore > blueScore && !messageshown)
          {
              for(Player player : Bukkit.getServer().getOnlinePlayers())
              {
                  TitleAPI.sendTitle(player, 20, 40, 20, ChatColor.RED + "Red" + ChatColor.YELLOW + " Team Wins!!!", "");
              }

              messageshown = true;
          }

          else if (redScore < blueScore && !messageshown)
          {
              for(Player player : Bukkit.getServer().getOnlinePlayers())
              {
                  TitleAPI.sendTitle(player, 20, 40, 20,ChatColor.BLUE + "Blue" + ChatColor.YELLOW + " Team Wins!!!", "");
              }

              messageshown = true;
          }

          else if (redScore == blueScore && !messageshown)
          {
              for(Player player : Bukkit.getServer().getOnlinePlayers())
              {
                  TitleAPI.sendTitle(player, 20, 40, 20,ChatColor.GREEN + "It's a draw", "");
              }

              messageshown = true;
          }

          if(postTime == 15)
          {
              for (Player player : Bukkit.getServer().getOnlinePlayers())
              {
                  ByteArrayDataOutput out = ByteStreams.newDataOutput();
                  out.writeUTF("Connect");
                  out.writeUTF("Minigame");
                  player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
                  //player.sendMessage("Teleport to bungee woosh");
              }
          }

          if(postTime == 30)
          {
              Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "restart");
          }


          postTime++;


      }







    }
}
